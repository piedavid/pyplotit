pyplotit: a python reimplementation of plotIt
=============================================

The plotIt_ tool was developed to efficiently produce large numbers of
stack plots that use the same set of samples.
It is a standalone C++ executable that is very good at what it does,
but it is not very customisable or flexbile: sometimes one "just" wants to
get a few histograms to make a specific plot instead of a whole batch, but
still take advantage of the information stored in the configuration file and
the naming conventions.

This package tries to bridge that gap: it aims to provide a simple python
interface to the de-facto file format defined by plotIt_: a YAML configuration
file and a director of ROOT_ files with histograms.
Basic plotting methods are provided, but they are currently far from supporting
all the styling options of plotIt_.

.. toctree::
   :hidden:

   intro
   apiref

.. toctree::
   :maxdepth: 2
   :titlesonly:
   :caption: Contents
   :glob:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. _plotIt: https://github.com/cp3-llbb/plotIt

.. _ROOT: https://root.cern
