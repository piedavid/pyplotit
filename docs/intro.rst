.. _gettingstarted:

Getting started
===============

Two command-line scripts are provided: ``iPlotIt`` and ``pyPlotIt``.
Both have a similar interface as the ``plotIt`` executable: they take a YAML
configuration file as a positional argument, and optional ``--histodir`` and
``--eras`` arguments, to pass a different histograms directory (in case they are
not in the same directory as the configuration file) and set of data-taking
periods (eras) to consider.
``pyPlotIt`` mimics the ``plotIt`` batch plot production, but is currently
not very useful, given the much more limited support for styling options.

``iPlotIt`` is the best place to get started: it loads a configuration
file and then opens an IPython_ shell to inspect it, and interactively load
and manipulate histograms.
Usually it can be used as

.. code-block:: bash

   iPlotIt plots.yml

The available objects are:

- ``config``, the :py:class:`~plotit.config.Configuration` object corresponding
  to the top level of the YAML file (excluding the sections that are parsed
  separately)
- ``samples``, a list of :py:class:`~plotit.plotit.Group` or ungrouped
  :py:class:`~plotit.plotit.File` objects (stateful, see below), which
  correspond to the ``groups`` and ``files`` sections of the configuration file
  and can be used to retrieve the histograms for a plot
- ``plots``, a list of :py:class:`~plotit.config.Plot` objects, which
  corresponds to the ``plots`` section of the configuration file
- ``systematics``, a list of systematic uncertainties
  (:py:class:`~plotit.plotit.SystVar` objects), which corresponds to
  the ``systematics`` section of the configuration file
- ``legend``, the parsed ``legend`` section, with the list of entries

From a script the same objects can be obtained by calling the
:py:meth:`~plotIt.plotIt.loadFromYAML` method.
There is one difference: this method returns a list of plots,
whereas ``iPlotIt`` provides a dictionary where each plot is stored
with its ``name`` attribute as a key |---| so they are equivalent,
the latter is only done for convenience.

.. _architecture:

Architecture
============

This package was designed to potentially replace plotIt_ in the long run,
so a few design choices were made with performance in mind, and others
slightly over-engineered to provide maximal flexibility for future
development.
The two main distinctions to keep in mind are between configuration and
stateful classes, and between raw histogram pointers and smart pointers.

The former is relatively straightforward, but causes some duplication:
the configuration file is initially parsed to classes that represent
the configuration, but carry no additional state; they are essentially
the dictionaries from the YAML parsing, but with some additional structure
based on the type information.
For many things this is sufficient, but for loading histograms from files
the files need to be opened, and for efficiency a pointer to the open file
should be stored.
This is why stateful :py:class:`~plotit.plotit.File` and
:py:class:`~plotit.plotit.Group` classes exist in :py:mod:`plotit.plotit`,
which carry the configuration-only part as their ``cfg`` attribute.

Smart histogram pointers are introduced for performance reasons: the most
time-consuming part of running ``plotIt`` in practice is opening ROOT_ files
and retrieving histograms (this can be hundreds of histograms spread out over
dozens of files for a single plot, with typical runs producing hundreds of
plots), and these histograms are also what drives the memory usage
when producing histograms in batch mode.
The :py:class:`~plotit.plotit.FileHist` class allows to control when histograms
are read from the file: it provides a handle to the histogram, but postpones
loading it from disk until the contents is first accessed.
It is also possible to force loading and unloading the ``TH1`` objects,
which allows a simple implementation of the strategy adopted by ``plotIt``,
where all histograms needed for a set of plots are loaded from each file in one
go, and cleaned up after the plots are produced.

:py:class:`~plotit.plotit.FileHist` is part of a class hierarchy, with
:py:class:`~plotit.plotit.BaseHist` defining the common interface and basic
functionality, and :py:class:`~plotit.plotit.MemHist` and
:py:class:`~plotit.plotit.SumHist` implementing the same interface as
:py:class:`~plotit.plotit.FileHist` for histograms that are not loaded from
a file and groups of histograms that should be added, respectively.
:py:class:`~plotit.plotit.Stack` is an extension of
:py:class:`~plotit.plotit.SumHist` that represents a stack of groups and files.
The common interface provides direct access to the ``TH1`` objects, as well as
access to the contents and ``sumw2`` arrays as NumPy_ arrays, which allows to
adopt a very pythonic style for implementing custom plots or other scripts.

.. _plotIt: https://github.com/cp3-llbb/plotIt

.. _ROOT: https://root.cern

.. _IPython: https://ipython.org

.. _NumPy: https://numpy.org

.. |---| unicode:: U+2014
   :trim:
