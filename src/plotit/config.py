"""
The :py:mod:`plotit.config` module contains classes that represent parts of
the YAML configuration file, and helper methods for parsing the different
blocks.
:py:class:`~plotit.config.Configuration`, :py:class:`~plotit.config.File`,
:py:class:`~plotit.config.Group`, :py:class:`~plotit.config.Configuration`,
:py:class:`~plotit.config.Label`, :py:class:`~plotit.config.Legend`,
:py:class:`~plotit.config.Plot`, :py:class:`~plotit.config.Point`, and
:py:class:`~plotit.config.Position` are the "stateless" classes mentioned
:ref:`here<architecture>`.
"""
__all__ = (
    "Group",
    "File",
    "Point",
    "Label",
    "Position",
    "Plot",
    "Legend",
    "Configuration",
    "parseWithIncludes",
    "loadPlots",
    "loadSystematics",
    "loadConfiguration",
    "loadFiles",
    "loadGroups",
    "loadLegend",
)
import dataclasses
import numbers
import os.path
from dataclasses import dataclass, field, fields
from typing import Any, Dict, List, Optional, Tuple, Union, cast

from .logging import logger


@dataclass
class BaseConfigObject:
    """ Base class for objects constructed from YAML """

    @staticmethod
    def _parse(cfg: Dict[str, Any]) -> Dict[str, Any]:
        return {(nm.replace("-", "_") if "-" in nm else nm): val for nm, val in cfg.items()}

    @classmethod
    def fromConfig(cls, cfg: Union[Dict[str, Any], List[float]], **kwargs: Any) -> Any:
        if isinstance(cfg, dict):
            if kwargs:  # merge, avoid changing cfg
                kwargs.update(cfg)
                cfg = kwargs
            cfg = cls._parse(cfg)
            clsFields = [f.name for f in fields(cls)]
            cfg = {k: v for k, v in cfg.items() if k in clsFields}
            return cls(**cfg)  # type: ignore
        else:
            return cls(*cfg)


Color = Tuple[float, float, float, float]


@dataclass
class SampleStyle(BaseConfigObject):
    type: str  #: MC, DATA, or SIGNAL
    name: str
    order: Optional[int] = None
    legend: str = ""
    legend_style: Optional[str] = None
    legend_order: int = 0
    drawing_options: str = ""
    marker_size: Optional[float] = None
    marker_color: Optional[Color] = None
    marker_type: Optional[int] = None
    fill_color: Optional[Color] = None
    fill_type: Optional[int] = None
    line_width: Optional[float] = None
    line_color: Optional[Color] = None
    line_type: Optional[int] = None

    def __post_init__(self) -> None:
        self.type = self.type.upper()

        if self.legend_style is None:
            if self.type == "MC":
                self.legend_style = "lf"
            elif self.type == "SIGNAL":
                self.legend_style = "l"
            elif self.type == "MC":
                self.legend_style = "pe"

        if self.drawing_options is None:
            if self.type in ("MC", "SIGNAL"):
                self.drawing_options = "hist"
            elif self.type == "DATA":
                self.drawing_options = "pe"

        black = SampleStyle.loadColor("#000000")
        if self.fill_color is None and self.type == "MC":
            self.fill_color = black
        if self.fill_type is None:
            if self.type == "MC":
                self.fill_type = 1001
            elif self.type == "SIGNAL":
                self.fill_type = 0
        if self.line_width is None:
            if self.type == "MC":
                self.line_width = 0
            else:
                self.line_width = 1
        if self.line_color is None and self.type == "MC":
            self.line_color = black
        if self.line_type is None and self.type == "SIGNAL":
            self.line_type = 2  # TODO something matplotlib also understands
        if self.marker_color is None and self.type == "DATA":
            self.marker_color = black  # TODO look up
        if self.type == "DATA":
            if self.marker_size is None:
                self.marker_size = 1
            if self.marker_type is None:
                self.marker_type = 20

    @staticmethod
    def _parse(cfg: Dict[str, Any]) -> Dict[str, Any]:
        cfg = BaseConfigObject._parse(cfg)
        for what in ("marker", "fill", "line"):
            argNm = f"{what}-color"
            if argNm in cfg:
                cfg[argNm] = SampleStyle.loadColor(cfg[argNm])
        return cfg

    @staticmethod
    def loadColor(color: str) -> Color:
        if not (color[0] == "#" and (len(color) == 7 or len(color) == 9)):
            raise ValueError(f"Color should be in the format '#RRGGBB' or '#RRGGBBAA' (got {color!r})")
        r = int(color[1:3], base=16) / 255.0
        g = int(color[3:5], base=16) / 255.0
        b = int(color[5:7], base=16) / 255.0
        a = int(color[7:9], base=16) / 255.0 if len(color) > 7 else 1.0
        return (r, g, b, a)


@dataclass
class Group(SampleStyle):
    files: "List[File]" = field(default_factory=list)

    @staticmethod
    def _parse(cfg: Dict[str, Any]) -> Dict[str, Any]:
        cfg = SampleStyle._parse(cfg)
        if "files" in cfg and "type" not in cfg:
            logger.debug(f"files: {cfg['files']!r}")
            f0type = next(f for f in cfg["files"]).type
            if not all(f.type == f0type for f in cfg["files"]):
                logger.warning(
                    "Not all the files with group {} have the same type: {}".format(
                        cfg.get("name"), ", ".join(f"{f.name}: {f.type}" for f in cfg["files"])
                    )
                )
            cfg["type"] = f0type
        return cfg


@dataclass
class File(SampleStyle):
    """ Configuration for a single sample """

    era: Optional[str] = None
    group: Optional[str] = None
    cross_section: float = 1.0
    branching_ratio: float = 1.0
    generated_events: float = 1.0
    scale: float = 1.0
    pretty_name: Optional[str] = None
    yields_title: Optional[str] = None
    yields_group: Optional[str] = None
    legend_group: Optional[str] = None

    def __post_init__(self) -> None:
        self.pretty_name = self.pretty_name or self.name
        if self.yields_group is None:
            if self.group is not None:
                self.yields_group = self.group
            elif self.legend is not None:
                self.yields_group = self.legend
            else:
                self.yields_group = self.name


@dataclass
class Point(BaseConfigObject):
    x: float
    y: float


@dataclass
class Label(BaseConfigObject):
    text: str
    position: Point
    size: int = 18


@dataclass
class Position(BaseConfigObject):
    x1: float
    y1: float
    x2: float
    y2: float


@dataclass
class Plot(BaseConfigObject):
    name: str
    exclude = ""
    x_axis: str = ""
    x_axis_format: str = ""
    y_axis: str = ""
    y_axis_format: Optional[str] = None
    normalized: bool = False
    no_data: bool = False
    override: bool = False
    log_y: bool = False
    log_x: bool = False
    save_extensions: List[str] = field(default_factory=list)
    book_keeping_folder: str = ""
    show_ratio: bool = False
    fit: bool = False
    fit_ratio: bool = False
    fit_function: str = ""
    fit_legend: str = ""
    fit_legend_position: Optional[Position] = None
    fit_range: Optional[List[float]] = None
    ratio_fit_function: str = ""
    ratio_fit_legend: str = ""
    ratio_fit_legend_position: Optional[Position] = None
    ratio_fit_range: Optional[List[float]] = None
    show_errors: bool = True
    x_axis_range: Optional[List[float]] = None
    y_axis_range: Optional[List[float]] = None
    log_y_axis_range: Optional[List[float]] = None
    ratio_y_axis_range: Optional[List[float]] = None
    blinded_range: Optional[List[float]] = None
    y_axis_show_zero: bool = False
    inherits_from: Optional[str] = None
    rebin: int = 1
    labels: List[Label] = field(default_factory=list)
    extra_label: Optional[str] = None
    legend_position: Optional[Position] = None
    legend_columns: int = 1
    show_overflow: bool = False
    errors_type: str = "Poisson"  # TODO get from global ?
    #
    binning_x: Optional[str] = None
    binning_y: Optional[str] = None
    draw_string: Optional[str] = None
    selection_string: Optional[str] = None
    #
    for_yields: bool = True
    yields_title: bool = True
    yields_table_order: bool = True
    sort_by_yields: bool = False
    #
    vertical_lines: List[float] = field(default_factory=list)
    horizontal_lines: List[float] = field(default_factory=list)
    lines: List[str] = field(default_factory=list)

    @staticmethod
    def _parse(cfg: Dict[str, Any]) -> Dict[str, Any]:
        cfg = BaseConfigObject._parse(cfg)
        if "labels" in cfg:
            cfg["labels"] = [Label.fromConfig(lblNd) for lblNd in cfg["labels"]]
        return cfg

    # def __post_init__(self):
    #     if self.x_axis_range is not None:
    #        try:
    #            lims = tuple(float(tok.strip()) for tok in self.x_axis_range.strip("[]").split(","))
    #            if len(lims) != 2:
    #                raise ValueError("")
    #        except Exception, e:
    #            raise ValueError("Could not parse x-axis-range {0}: {1}".format(self.x_axis_range, e))
    #        self.x_axis_range = lims


@dataclass
class Legend(BaseConfigObject):
    position: Position = Position(x1=0.6, y1=0.6, x2=0.9, y2=0.9)
    columns: int = 1
    entries: "List[Legend.Entry]" = field(default_factory=list)

    @dataclass
    class Entry(BaseConfigObject):
        label: str
        type: str = "MC"
        order: int = 0

    @staticmethod
    def _parse(cfg: Dict[str, Any]) -> Dict[str, Any]:
        cfg = BaseConfigObject._parse(cfg)
        if "position" in cfg:
            cfg["position"] = Position.fromConfig(cfg["position"])
        if "entries" in cfg:
            cfg["entries"] = [Legend.Entry.fromConfig(entryNd) for entryNd in cfg["entries"]]
        return cfg


@dataclass
class Configuration(BaseConfigObject):
    width: int = 800
    height: int = 800
    margin_left: float = 0.17
    margin_right: float = 0.03
    margin_top: float = 0.05
    margin_bottom: float = 0.13
    #
    eras: List[str] = field(default_factory=list)
    luminosity: Dict[str, float] = field(default_factory=dict)
    scale: float = 1.0
    no_lumi_rescaling: bool = False
    luminosity_error: float = 0.0
    #
    y_axis_format: str = "%1% / %2$.2f"  # TODO make this a python format
    ratio_y_axis_title: str = "Data / MC"
    ratio_style: str = "P0"  # TODO ???
    #
    error_fill_color: int = 42  # TODO ???
    error_fill_style: int = 3154  # TODO ???
    #
    fit_n_points: int = 1000
    fit_line_color: int = 46  # TODO ???
    fit_line_width: int = 1  # TODO ???
    fit_line_style: int = 1  # TODO ???
    fit_error_fill_color: int = 42  # TODO ???
    fit_error_fill_style: int = 1001  # TODO ???
    ratio_fit_n_points: int = 1000
    ratio_fit_line_color: int = 46  # TODO ???
    ratio_fit_line_width: int = 1  # TODO ???
    ratio_fit_line_style: int = 1  # TODO ???
    ratio_fit_error_fill_color: int = 42  # TODO ???
    ratio_fit_error_fill_style: int = 1001  # TODO ???
    # TODO line_style
    # , labels # TODO
    experiment: str = "CMS"
    extra_label: str = ""
    luminosity_label: str = ""
    root: str = "."
    #
    transparent_background: bool = False
    show_overflow: bool = False  # TODO default for plot
    errors_type: str = "Poisson"  # TODO define types
    x_axis_label_size: int = 18
    y_axis_label_size: int = 18
    x_axis_top_ticks: bool = True
    y_axis_right_ticks: bool = True
    blinded_range_fill_color: int = 42  # TODO ???
    blinded_range_fill_style: int = 1001  # TODO ???
    #
    yields_table_stretch: float = 1.15
    yields_table_align: str = "h"  # TODO this one
    yields_table_text_align: str = "c"
    yields_table_numerical_precision_yields: int = 1
    yields_table_numerical_precision_ratio: int = 2

    def getLumi(self, eras: Optional[List[str]] = None) -> float:
        if eras is None:
            if isinstance(self.luminosity, numbers.Number):
                return self.luminosity
            else:
                return sum(eraLumi for eraLumi in self.luminosity.values())
        else:
            return sum(self.luminosity[era] for era in eras)


def _plotit_loadWrapper(fpath: str) -> Any:
    """ yaml.safe_load from path """
    import yaml

    with open(fpath) as f:
        res = yaml.safe_load(f)
    return res


def _load_includes(cfgDict: Dict[str, Any], basePath: str) -> None:
    updDict = dict()
    for k, v in cfgDict.items():
        if isinstance(v, dict):
            if len(v) == 1 and next(k for k in v) == "include":
                vals = v[next(k for k in v)]
                newDict = dict()
                for iPath in vals:
                    if not os.path.isabs(iPath):
                        iPath = os.path.join(basePath, iPath)
                    if not os.path.exists(iPath):
                        raise OSError(f"Included path '{iPath}' does not exist")
                    newDict.update(_plotit_loadWrapper(iPath))
                updDict[k] = newDict
                _load_includes(newDict, basePath)
            else:
                _load_includes(v, basePath)
    cfgDict.update(updDict)


def parseWithIncludes(yamlPath: str) -> Any:
    """ Parse a YAML file, with 'includes' relative to its path """
    cfg = _plotit_loadWrapper(yamlPath)
    basedir = os.path.dirname(yamlPath)
    _load_includes(cfg, basedir)
    return cfg


# Consistent set of helpers to load config objects from config dictionaries as found in the YAML


def mergeDicts(first: Dict[Any, Any], second: Dict[Any, Any]) -> Dict[Any, Any]:
    """ trivial helper: copy first, update with second and return """
    md = dict(first)
    md.update(second)
    return md


def loadPlots(
    plotsConfig: Optional[Dict[str, Dict[str, Any]]], defaultStyle: Union[None, Configuration, SampleStyle] = None
) -> List[Plot]:
    """ Load a list of :py:class:`~plotit.config.Plot` instances from a config dictionary """
    if plotsConfig is None:
        return []
    plotDefaults = {}
    if defaultStyle:
        plotDefaults = {
            field.name: getattr(defaultStyle, field.name)
            for field in dataclasses.fields(SampleStyle)
            if field.default != dataclasses.MISSING and hasattr(defaultStyle, field.name)
        }
    return [
        Plot.fromConfig((mergeDicts(plotDefaults, pConfig) if plotDefaults else pConfig), name=pName)
        for pName, pConfig in plotsConfig.items()
    ]


def loadSystematics(systConfigs: List[Dict[str, Any]], configuration: Configuration) -> List[Any]:
    """ Load a list of :py:class:`~plotit.systematics.SystVar` instances from config entries"""
    systs = []
    if systConfigs is not None:
        from .systematics import parseSystematic

        systs += [parseSystematic(item) for item in systConfigs]
    # lumi systematic
    if configuration and configuration.luminosity_error != 0.0:
        from .systematics import ConstantSystVar

        lumisyst = ConstantSystVar("lumi", 1.0 + configuration.luminosity_error, pretty_name="Luminosity")
        logger.debug(f"Adding luminosity systematic {lumisyst!r}")
        systs.append(lumisyst)
    return systs


def loadConfiguration(confConfig: Optional[Dict[str, Any]] = None) -> Configuration:
    """ Load a :py:class:`~plotit.config.Configuration` from a dict """
    if confConfig is None:
        confConfig = dict()
    return cast(Configuration, Configuration.fromConfig(confConfig))


def loadFiles(fileConfigs: Optional[Dict[str, Any]] = None) -> List[File]:
    """ Load a list of :py:class:`~plotit.config.File` instances from a dictionary with settings """
    if fileConfigs is None:
        return []
    return [File.fromConfig(fileCfg, name=name) for name, fileCfg in fileConfigs.items()]


def loadGroups(
    groupConfigs: Optional[Dict[str, Any]] = None, files: Optional[List[File]] = None, includeEmpty: bool = False
) -> List[Group]:
    """ Load a list of :py:class:`~plotit.config.Group` instances from a dictionary with settings """
    if groupConfigs is None:
        return []
    if files is None:
        files = []
    groups = []
    for name, groupCfg in groupConfigs.items():
        groupFiles = [f for f in files if f.group == name]
        if includeEmpty or groupFiles:
            groups.append(Group.fromConfig(groupCfg, name=name, files=groupFiles))
    return groups


def loadLegend(legendConfig: Optional[Dict[str, Any]] = None) -> Legend:
    """ Load a :py:class:`~plotit.config.Legend` instance from a dictionary with settings """
    if legendConfig is None:
        legendConfig = dict()
    return cast(Legend, Legend.fromConfig(legendConfig))
