"""
Python version of plotIt, for plotting with ROOT or matplotlib

Based on https://github.com/cp3-llbb/plotIt

WARNING: very much work-in-progress, many things are not implemented yet
"""
import argparse
import numbers
from collections.abc import Iterable, Iterator, Mapping
from functools import partial
from itertools import chain, islice
from typing import (
    TYPE_CHECKING,
    Any,
    Callable,
    Dict,
    List,
    Optional,
    Sequence,
    Tuple,
    Union,
    cast,
)

import numpy as np

from . import config
from . import histo_utils as h1u
from .logging import logger


class lazyload(property):
    """
    Python2 equivalent of a read-only functools.lazyload

    On top of being available also for python before 3.8,
    having a separate class helps e.g. with optimisation
    (it is possible to identify all of these to group the loads,
    and clean up the objects).

    If the host class uses __slots__, the appropriate attribute
    must be defined there (name prefix with an underscore), and
    initialized (usually to ``None``) in the constructor.
    """

    __slots__ = ("_cName",)

    def __init__(self, fget: Callable[[Any], Any]) -> None:
        super().__init__(fget=fget, doc=property.__doc__)
        self._cName = f"_{fget.__name__}"

    def __get__(self, instance: Any, *args: Any) -> Any:
        value = getattr(instance, self._cName)
        if value is None:
            value = self.fget(instance)  # type: ignore
            setattr(instance, self._cName, value)
        return value


class BaseHist:
    __slots__ = ("_obj", "_shape", "_contents", "_sumw2")

    def __init__(self) -> None:
        self._obj: Any = None
        self._shape: Tuple[int, ...] = tuple()
        self._contents: Optional[np.ndarray] = None
        self._sumw2: Optional[np.ndarray] = None

    def nBins(self, axis: int) -> int:
        """ Number of (non-overflow) bins for an axis """
        return self.shape[axis] - 2

    @lazyload
    def obj(self) -> Any:
        return None

    @property
    def shape(self) -> Tuple[int, ...]:
        return self._shape

    @lazyload
    def contents(self) -> np.ndarray:
        """ Default implementation: view of the array in obj """
        return h1u.tarr_asnumpy(self.obj, shape=self.shape)

    @lazyload
    def sumw2(self) -> np.ndarray:
        """ Default implementation: view of the array in obj """
        if h1u.hasSumw2(self.obj):
            return h1u.tarr_asnumpy(self.obj.GetSumw2(), shape=self.shape)
        else:
            return self.contents

    def contributions(self) -> "Iterator[BaseHist]":
        """ Default: just self """
        yield self

    def getCombinedSyst2(self, systematics: "Iterable[str]") -> np.ndarray:  # TODO move to BaseHist
        """ Calculate the combined systematic uncertainty on the sum """
        return getCombinedSyst2(list(self.contributions()), systematics)

    # TODO access axes / binnings

    def getStyleOpt(self, name: str) -> Any:
        pass  # only for FileHist


class SystVar:
    """ interface & base for a systematic variation (without specified histogram) """

    @staticmethod
    def default_filter(fName: str, fObj: config.File) -> bool:
        """ returns True for any file name and pointer """
        return True

    def __init__(
        self, name: str, pretty_name: Optional[str] = None, on: Callable[[str, config.File], bool] = lambda str, x: True
    ) -> None:
        """Constructor

        Arguments:
          name          systematic name (used internally and in names)
          pretty_name   pretty name (used in places where formatting is possible)
          on            a callable that takes a file name and argument, and returns
                        True if the systematic should be evaluated for that sample
        """
        self.name = name
        self.pretty_name = pretty_name if pretty_name is not None else name
        self.on = on
        super().__init__()

    def _repr_args(self) -> List[str]:
        return [self.name]

    def _repr_kwargs(self) -> List[str]:
        return (["pretty_name"] if self.pretty_name != self.name else []) + (
            ["on"] if self.on != SystVar.default_filter else []
        )

    def __repr__(self) -> str:
        return "{}({})".format(
            self.__class__.__name__,
            ", ".join(
                chain(
                    (repr(a) for a in self._repr_args()),
                    ("{}={!r}".format(k, getattr(self, k)) for k in self._repr_kwargs()),
                )
            ),
        )

    def forHist(self, hist: BaseHist) -> "SystVar.ForHist":
        """ get variation for hist """
        return self.__class__.ForHist(hist, self)

    class ForHist:
        """ Interface & base for systematic variation for a single histogram """

        __slots__ = ("hist", "systVar")

        def __init__(self, hist: BaseHist, systVar: "SystVar") -> None:
            self.hist = hist
            self.systVar = systVar

        @property
        def nom(self) -> np.ndarray:
            """ Nominal values """
            return self.hist.contents

        @property
        def up(self) -> np.ndarray:
            """ Up variation values """
            return self.hist.contents

        @property
        def down(self) -> np.ndarray:
            """ Down variation values """
            return self.hist.contents


if TYPE_CHECKING:
    SystVarsForHistBase = Mapping[str, SystVar.ForHist]
else:
    SystVarsForHistBase = Mapping


class SystVarsForHist(SystVarsForHistBase):
    """dict-like object to assign as systVars to an entry

    (parent is the actual dictionary with SystVars)"""

    __slots__ = ("hist", "parent", "_cached")

    def __init__(self, hist: BaseHist, parent: Dict[str, SystVar]) -> None:
        self.hist = hist
        self.parent = parent
        self._cached: Dict[str, SystVar.ForHist] = dict()

    def __getitem__(self, ky: str) -> SystVar.ForHist:
        if ky in self._cached:
            return self._cached[ky]
        else:
            fp = self.parent[ky].forHist(self.hist)
            if isinstance(fp, SystVar.ForHist):
                self._cached[ky] = fp
            return fp

    def __iter__(self) -> "Iterator[str]":
        yield from self.parent

    def __len__(self) -> int:
        return len(self.parent)


class File:
    """
    plotIt sample or file, since all plots for a sample are in one ROOT file.

    This object mainly holds the open TFile pointer, normalisation scale,
    and systematics dictionary for the sample. The configuration (from YAML)
    parameteres are collected in a :py:class:`plotit.config.File` instance,
    as the `cfg` attribute.
    """

    __slots__ = ("_tFile", "name", "path", "cfg", "scale", "systematics")

    def __init__(
        self, name: str, path: str, fCfg: config.File, config: config.Configuration, systematics: List[SystVar]
    ):
        self.name = name
        self.path = path
        self.cfg = fCfg
        self._tFile: Any = None
        self.scale = File._getScale(self.cfg, config)
        self.systematics: Dict[str, SystVar] = {
            syst.name: syst for syst in systematics if self.cfg.type != "DATA" and syst.on(self.name, self.cfg)
        }
        logger.debug("Scale for file {0.name}: {0.scale:e}; systematics: {0.systematics!s}".format(self))

    @lazyload
    def tFile(self) -> Any:
        from cppyy import gbl

        tf = gbl.TFile.Open(self.path)
        if not tf:
            logger.error(f"Could not open file {self.path}")
        return tf

    def getHist(
        self, plot: config.Plot, name: Optional[str] = None, eras: Union[None, str, List[str]] = None
    ) -> "FileHist":
        """ Get the histogram for the combination of ``plot`` and this file/sample """
        hk = FileHist(self, plot, name=name)
        hk.systVars = SystVarsForHist(hk, self.systematics)
        return hk

    @staticmethod
    def _getScale(fCfg: config.File, config: config.Configuration) -> float:
        """ Infer the scale factor for histograms from the file dict and the overall config """
        if fCfg.type == "DATA":
            return 1.0
        else:
            if fCfg.era:
                lumi = config.luminosity[fCfg.era]
            elif isinstance(config.luminosity, numbers.Number):
                lumi = config.luminosity
            else:
                lumi = sum(lumi for era, lumi in config.luminosity.items())
            mcScale = lumi * fCfg.cross_section * fCfg.branching_ratio / fCfg.generated_events
            return mcScale * config.scale * fCfg.scale

    def __repr__(self) -> str:
        return "File({0.path!r}, scale={0.scale}, systematics={0.systematics!r})".format(self)


class Group:
    """Group of samples

    Similar interface as :py:class:`~plotit.plotit.File`, based on a list of those.
    """

    __slots__ = ("name", "files", "cfg")

    def __init__(self, name: str, files: Sequence[File], gCfg: config.Group):
        self.name = name
        self.cfg = gCfg
        self.files = files

    def getHist(self, plot: config.Plot, eras: Union[None, str, List[str]] = None) -> "GroupHist":
        """ Get the histogram for the combination of ``plot`` and this group of samples """
        hists = []
        for f in self.files:
            if eras is None:
                hists.append(f.getHist(plot))
            elif isinstance(eras, str):
                if eras == f.cfg.era:
                    hists.append(f.getHist(plot))
            else:  # eras is a list
                if f.cfg.era in eras:
                    hists.append(f.getHist(plot))
        return GroupHist(self, hists)

    def __repr__(self) -> str:
        return "Group({0.name!r}, {0.files!r})".format(self)


class MemHist(BaseHist):
    """ In-memory histogram, minimally compatible with :py:class:`~plotit.plotit.FileHist` """

    def __init__(self, obj: Any):
        self._obj = obj
        self._shape = h1u.getShape(obj)


class FileHist(BaseHist):
    """
    Histogram from a file, or distribution for one sample.

    The actual object is only read into memory when first used.
    Transformations like scaling and rebinning are also applied then.
    In addition, references to the sample :py:class:`~plotit.plotit.File`
    and :py:class:`~plotit.config.Plot` are held.
    """

    __slots__ = ("name", "_tFile", "plot", "hFile", "systVars", "_syst2")

    def __init__(
        self,
        hFile: File,
        plot: config.Plot,
        name: Optional[str] = None,
        tFile: Any = None,
        systVars: Optional[SystVarsForHist] = None,
    ):
        """Histogram key constructor. The object is read on first use, and cached.

        :param hFile:   :py:class:`plotit.plotit.File` instance corresponding to the sample
        :param plot:    :py:class:`~plotit.config.Plot` configuration
        :param name:    name of the histogram inside the file (taken from ``plot`` if not specified)
        :param tFile:   ROOT file with histograms (taken from ``hFile`` if not specified)
        """
        super().__init__()
        self.name = name or plot.name
        self._tFile = tFile  # can be explicitly passed, or None (taken from hFile on first use then)
        self.plot = plot
        self.hFile = hFile
        self.systVars = systVars or SystVarsForHist(self, dict())
        self._syst2: Optional[np.ndarray] = None

    @property
    def tFile(self) -> Any:  # only called if not constructed explicitly
        return self.hFile.tFile

    def __str__(self) -> str:
        return f'FileHist("{self.tFile.GetName()}", "{self.name}")'

    def clone(
        self,
        hFile: Optional[File] = None,
        plot: Optional[config.Plot] = None,
        name: Optional[str] = None,
        tFile: Optional[Any] = None,
        systVars: Optional[SystVarsForHist] = None,
    ) -> "FileHist":
        """ Modifying clone method. `systVars` is *not* included by default """
        return FileHist(
            (hFile if hFile is not None else self.hFile),
            (plot if plot is not None else self.plot),
            name=(name if name is not None else self.name),
            tFile=(tFile if tFile is not None else self.tFile),
            systVars=systVars,
        )

    @lazyload
    def obj(self) -> Any:
        """ the underlying TH1 object """
        # load the object from the file, and apply transformations as needed
        if (not self.tFile) or self.tFile.IsZombie() or (not self.tFile.IsOpen()):
            raise RuntimeError(f"File '{self.tFile}'cannot be read")
        res = self.tFile.Get(self.name)
        if not res:
            raise KeyError(
                "Could not retrieve key '{}' from file {}".format(
                    self.name, (self.tFile.GetName() if self.tFile else repr(self.tFile))
                )
            )
        # scale/rebin/crop if needed
        scale = self.hFile.scale
        rebin = self.plot.rebin
        xOverflowRange = self.plot.x_axis_range
        if (scale != 1.0) or (rebin != 1) or (xOverflowRange is not None):
            res = h1u.cloneHist(res)
            if xOverflowRange is not None:
                res.GetXaxis().SetRangeUser(xOverflowRange[0], xOverflowRange[1])
                h1u.addOverflow(res, res.GetXaxis().GetFirst(), True)
                h1u.addOverflow(res, res.GetXaxis().GetLast(), False)
            if scale != 1.0:
                if not h1u.hasSumw2(res):
                    res.Sumw2()  # NOTE should be automatic
                res.Scale(scale)
            if rebin != 1:
                res.Rebin(rebin)
            # NOTE If scale/rebin are done in numpy (or later, when drawing), the copy of the histogram may be avoided
        self._shape = h1u.getShape(res)  # TODO take overflow into account ?
        return res

    @property
    def shape(self) -> Tuple[int, ...]:
        self.obj  # load if needed
        return self._shape

    def getStyleOpt(self, name: str) -> Any:
        return getattr(self.hFile.cfg, name)

    def __repr__(self) -> str:
        return "FileHist({0.name!r}, {0.hFile!r})".format(self)

    @lazyload
    def syst2(self) -> np.ndarray:
        return self.getCombinedSyst2(self.systVars.keys())


class SumHist(BaseHist):
    """
    Histogram that is the sum of other histograms (used for stack total, base of GroupHist)
    """

    __slots__ = ("entries", "_syst2")

    def __init__(self, entries: Sequence[BaseHist]) -> None:
        super().__init__()
        self.entries: List[BaseHist] = []
        self._syst2: Optional[np.ndarray] = None
        if entries:
            for entry in entries:
                self.add(entry, clearTotal=False)

    def add(self, entry: BaseHist, clearTotal: bool = True) -> None:
        """ Add an entry to the sum (clearing the total, if already built) """
        # TODO some kind of axis compatibility test (delayed, then, in contributions() or its users)
        self.entries.append(entry)
        if clearTotal:
            self.clear()

    def clear(self) -> None:
        if self._contents is not None:
            self._contents = None
        if self._sumw2 is not None:
            self._sumw2 = None
        if self._syst2 is not None:
            self._syst2 = None
        if self._obj is not None:
            self._obj = None

    @property
    def shape(self) -> Tuple[int, ...]:
        if not self._shape:  # retrieve (and possibly load) if needed
            self._shape = self.entries[0].shape
        return self._shape

    @lazyload
    def obj(self) -> Any:
        """ the underlying TH1 object """
        res = h1u.cloneHist(self.entries[0].obj)
        for entry in islice(self.entries, 1, None):
            res.Add(entry.obj)
        return res

    def contributions(self) -> "Iterator[BaseHist]":
        for entry in self.entries:
            yield from entry.contributions()

    @lazyload
    def contents(self) -> np.ndarray:
        return sum(contrib.contents for contrib in self.contributions())

    @lazyload
    def sumw2(self) -> np.ndarray:
        return sum(contrib.sumw2 for contrib in self.contributions())

    @lazyload
    def syst2(self) -> np.ndarray:
        return self.getCombinedSyst2(
            set(
                chain.from_iterable(
                    contrib.systVars for contrib in self.contributions() if isinstance(contrib, FileHist)
                )
            )
        )


class GroupHist(SumHist):
    """
    Combined histogram for a group of samples (SumHist with a config)
    """

    __slots__ = ("group",)

    def __init__(self, group: Group, entries: Sequence[BaseHist]) -> None:
        """Constructor

        :param group: :py:class:`~plotit.plotit.Group` instance
        :param entries: a :py:lass:`~plotit.plotit.FileHist` for each sample in the group
        """
        super().__init__(entries)
        self.group = group
        # TODO is self.plot needed? explicitly or as property

    def getStyleOpt(self, name: str) -> Any:
        return getattr(self.group.cfg, name)


def getCombinedSyst2(contributions: Union[BaseHist, Sequence[BaseHist]], systematics: "Iterable[str]") -> np.ndarray:
    """Get the combined systematic uncertainty

    :param contributions: contributions (FileHist) to group over
    :param systematics:  systematic variations to consider (if None, all that are present are used for each histogram)
    """
    contribs = [cast(BaseHist, contributions)] if not isinstance(contributions, list) else contributions
    shape = contribs[0].shape
    assert all(cb.shape == shape for cb in contribs)
    syst2 = np.zeros(shape)
    for syst in systematics:
        systInBins = np.zeros(shape)
        for contrib in contribs:
            svh = contrib.systVars.get(syst)
            if svh:
                systInBins += np.maximum(np.abs(svh.up - svh.nom), np.abs(svh.nom - svh.down))
        syst2 += systInBins ** 2
    return syst2


class Stack(SumHist):
    """
    Stack of distribution contributions from different samples.

    The entries are instances of either :py:class:`~plotit.plotit.FileHist` or
    :py:class:`~plotit.plotit.GroupHist`, for a single sample or a group of them,
    respectively.
    In addition to summing the histograms, helper methods to calculate the
    systematic uncertainty on the total, combined with the statistical uncertainty
    or not, are provided.
    """

    def __init__(self, entries: Optional[List[BaseHist]] = None) -> None:
        super().__init__(entries=entries or [])

    # TODO absorbe in the draw backends, simple enough now
    def getSystematicHisto(self) -> Any:
        """ construct a histogram of the stack total, with only systematic uncertainties """
        return h1u.h1With(self.obj, errors2=self.syst2)

    def getStatSystHisto(self) -> Any:
        """ construct a histogram of the stack total, with statistical+systematic uncertainties """
        return h1u.h1With(self.obj, errors2=(self.sumw2 + self.syst2))

    def getRelSystematicHisto(self) -> Any:
        """ construct a histogram of the relative systematic uncertainties for the stack total """
        # relSyst2 = self.syst2 / h1u.tarr_asnumpy(self.total)**2
        return h1u.h1With(self.obj, values=np.ones(self.shape), errors2=(self.syst2 / self.contents ** 2))

    # TODO check if this still makes sense / is needed
    @staticmethod
    def merge(*stacks: "Stack") -> "Stack":
        """ Merge two or more stacks """
        if len(stacks) < 2:
            return stacks[0]
        else:
            mergedSt = Stack()
            for i, entry in enumerate(stacks[0].entries):
                newHist = h1u.cloneHist(entry.obj)
                for stck in islice(stacks, 1, None):
                    newHist.Add(stck.entries[i].obj)
                mergedSt.add(MemHist(newHist))  # systVars=entry.systVars)
            return mergedSt


def loadHistograms(histograms: List[BaseHist]) -> None:
    """
    Efficiently load all histograms from disk

    :param histograms: nominal histograms iterable. The systematic variations histograms are also found and loaded.
    """
    from collections import defaultdict

    from .systematics import ShapeSystVar

    h_per_file = defaultdict(list)
    for hk in histograms:
        if isinstance(hk, FileHist):
            h_per_file[hk.hFile.path].append(hk)
    nOKTot, nFailTot = 0, 0
    for fPath, fHKs in h_per_file.items():
        nOK, nFail = 0, 0
        for hk in fHKs:
            if hk.obj is not None:  # trigger load
                nOK += 1
            else:
                nFail += 1
            if hk.systVars:
                for sv in hk.systVars.values():
                    if isinstance(sv, ShapeSystVar.ForHist):
                        if sv.histUp.obj is not None:
                            nOK += 1
                        else:
                            nFail += 1
                        if sv.histDown.obj is not None:
                            nOK += 1
                        else:
                            nFail += 1
        logger.debug(f"Loaded {nOK:d} histograms from file {fPath} ({nFail:d} failed)")
        nOKTot += nOK
        nFailTot += nFail
    logger.debug("Loaded {:d} histograms from {:d} files ({:d} failed)".format(nOKTot, len(h_per_file), nFailTot))


def clearHistograms(histograms: List[BaseHist]) -> None:
    """
    Clear all histograms

    :param histograms: nominal histograms iterable. The systematic variations histograms are also found and loaded.
    """
    from .systematics import ShapeSystVar

    nCleared, nEmpty = 0, 0
    for hk in histograms:
        if isinstance(hk, FileHist):
            if hk._obj:
                del hk._obj
                nCleared += 1
            else:
                nEmpty += 1
            if hk.systVars:
                for sv in hk.systVars.values():
                    if isinstance(sv, ShapeSystVar.ForHist):
                        if sv.histUp._obj:
                            del sv.histUp._obj
                            nCleared += 1
                        else:
                            nEmpty += 1
                        if sv.histDown._obj:
                            del sv.histDown._obj
                            nCleared += 1
                        else:
                            nEmpty += 1
    logger.debug(f"Cleared {nCleared:d} histograms from memory ({nEmpty:d} were not loaded)")


def makeStackRatioPlots(
    plots: List[config.Plot],
    samples: List[Union[Group, File]],
    systematics: Optional[List[SystVar]] = None,
    config: Optional[config.Configuration] = None,
    outdir: str = ".",
    backend: str = "matplotlib",
    chunkSize: int = 100,
    luminosity: float = 0.0,
) -> None:
    """
    Draw a traditional plotIt plot: data and MC stack, with (optional) signal distributions and ratio below

    :param plots: list of plots to draw
    :param samples: samples (groups and files) to consider
    :param systematics: selected systematics (TODO: implement this)
    :param config: global config (TODO is this necessary, or can it all be put in the plot config?)
    :param outdir: output directory
    :param backend: backend (ROOT or matplotlib)
    """
    # default kwargs
    if backend == "matplotlib":
        from .draw_mpl import drawStackRatioPlot
    elif backend == "ROOT":
        from .draw_root import drawStackRatioPlot  # type: ignore
    else:
        raise ValueError(f"Unknown backend: {backend!r}, valid choices are 'ROOT' and 'matplotlib'")

    dataSamples = [smp for smp in samples if smp.cfg.type == "DATA"]
    mcSamples = [smp for smp in samples if smp.cfg.type == "MC"]
    signalSamples = [smp for smp in samples if smp.cfg.type == "SIGNAL"]

    # build stacks
    stacks_per_plot = [
        (
            plot,
            (
                Stack(entries=[smp.getHist(plot) for smp in dataSamples]),
                Stack(entries=[smp.getHist(plot) for smp in mcSamples]),
                [smp.getHist(plot) for smp in signalSamples],
            ),
        )
        for plot in plots
    ]
    logger.debug("Drawing {:d} plots, splitting in chunks of {:d}".format(len(plots), chunkSize))
    # load and draw, in chunks
    for i in range(0, len(stacks_per_plot), chunkSize):
        i_stacks_plots = stacks_per_plot[i : i + chunkSize]
        allChunkContribs = list(
            chain.from_iterable(
                chain(dataStack.contributions(), mcStack.contributions(), sigHists)
                for plot, (dataStack, mcStack, sigHists) in i_stacks_plots
            )
        )
        loadHistograms(allChunkContribs)
        for plot, (dataStack, mcStack, _) in i_stacks_plots:
            logger.debug(f"Drawing plot {plot.name}")
            drawStackRatioPlot(plot, mcStack, dataStack, outdir=outdir, config=config, luminosity=luminosity)
        clearHistograms(allChunkContribs)


def getHistoPath(histoPath: str, cfgRoot: str = ".", baseDir: str = ".") -> str:
    import os.path

    if os.path.isabs(histoPath):
        return histoPath
    elif os.path.isabs(cfgRoot):
        return os.path.join(cfgRoot, histoPath)
    else:
        return os.path.join(baseDir, cfgRoot, histoPath)


def samplesFromFilesAndGroups(
    allFiles: List[File], groupConfigs: List[config.Group], eras: Optional[List[str]] = None
) -> List[Union[File, Group]]:
    """ Group and sort files that should be included for a era (or a set of eras) """
    from collections import defaultdict

    files_by_group = defaultdict(list)
    groups_and_samples: List[Union[File, Group]] = []
    for fl in allFiles:
        if eras is None or fl.cfg.era in eras:
            if fl.cfg.group:
                try:
                    files_by_group[fl.cfg.group].append(fl)
                except StopIteration:
                    logger.warning("Group {0.cfg.group!r} of sample {0.name!r} not found, adding ungrouped".format(fl))
                    groups_and_samples.append(fl)
            else:
                groups_and_samples.append(fl)
    groups_and_samples += [
        Group(gCfg.name, files_by_group[gCfg.name], gCfg) for gCfg in groupConfigs if gCfg.name in files_by_group
    ]
    return sorted(groups_and_samples, key=lambda f: f.cfg.order if f.cfg.order is not None else 0)


def samplesForEras(samples: List[Union[File, Group]], eras: Optional[List[str]] = None) -> List[Union[File, Group]]:
    """ Reduce a list of samples (files and groups) to those that should be included for a specific era (or a set of eras) """
    if isinstance(eras, str):
        eras = [eras]
    selSamples = []
    for smp in samples:
        if eras is None:
            selSamples.append(smp)
        elif isinstance(smp, File):
            if smp.cfg.era is None or smp.cfg.era in eras:
                selSamples.append(smp)
        elif isinstance(smp, Group):
            eraFiles = [f for f in smp.files if f.cfg.era is None or f.cfg.era in eras]
            if eraFiles:
                selSamples.append(Group(smp.name, eraFiles, smp.cfg))
    return selSamples


def resolveFiles(
    cFiles: List[config.File],
    config: config.Configuration,
    systematics: Optional[List[SystVar]] = None,
    histodir: str = ".",
) -> List[File]:
    """ Resolve paths, and construct :py:class:`plotit.plotit.File` objects (with TFile handles) from :py:class:`plotit.config.File` objects (pure configuration) """
    resolve = partial(getHistoPath, cfgRoot=config.root, baseDir=histodir)
    return [File(fCfg.name, resolve(fCfg.name), fCfg, config, systematics=(systematics or [])) for fCfg in cFiles]


def loadFromYAML(
    yamlFileName: str, histodir: str = ".", eras: Union[None, str, List[str]] = None
) -> Tuple[config.Configuration, List[Union[File, Group]], List[config.Plot], List[SystVar], config.Legend]:
    """
    Parse a plotIt YAML file. The returned objects are stateless,
    except for the list of samples, which consists of stateful
    :py:class:`~plotit.plotit.Group` or ungrouped
    :py:class:`~plotit.plotit.File` instances that carry a ``TFile`` pointer.

    :param yamlFileName: config file path
    :param histodir: base path for finding histograms (to be combined with ``'root'`` in configuration and the sample file names)
    :param eras: selected era, or list of selected eras (default: all that are present)
    """
    from .config import parseWithIncludes

    yCfg = parseWithIncludes(yamlFileName)
    # create config objects from YAML dictionary
    from .config import (
        loadConfiguration,
        loadFiles,
        loadGroups,
        loadLegend,
        loadPlots,
        loadSystematics,
    )

    config = loadConfiguration(yCfg.get("configuration"))
    cFiles = loadFiles(yCfg.get("files"))
    cGroups = loadGroups(yCfg.get("groups"), files=cFiles)
    plots = loadPlots(yCfg.get("plots"), config)
    systematics = loadSystematics(yCfg.get("systematics"), configuration=config)
    legend = loadLegend(yCfg.get("legend"))
    # resolve, select, and sort files and groups into samples
    files = resolveFiles(cFiles, config, systematics=systematics, histodir=histodir)
    if eras is None:
        eras = config.eras
    elif isinstance(eras, str):
        eras = [eras]
    samples = samplesFromFilesAndGroups(files, cGroups, eras=eras)
    return config, samples, plots, systematics, legend


def plotItFromYAML(
    yamlFileName: str,
    histodir: str = ".",
    outdir: str = ".",
    eras: Optional[List[str]] = None,
    backend: str = "matplotlib",
) -> None:
    logger.info(f"Running like plotIt with config {yamlFileName}, histodir={histodir}, outdir={outdir}")
    config, samples, plots, systematics, legend = loadFromYAML(yamlFileName, histodir=histodir, eras=eras)
    makeStackRatioPlots(
        plots,
        samples,
        systematics=systematics,
        config=config,
        outdir=outdir,
        backend=backend,
        luminosity=config.getLumi(eras),
    )


def makeBaseArgsParser(description: Optional[str] = None) -> argparse.ArgumentParser:
    def optStrList(value: str) -> Optional[Sequence[str]]:
        if value:
            return value.split(",")
        else:
            return None

    parser = argparse.ArgumentParser(description=description)
    parser.add_argument("yamlFile", help="plotIt configuration file (e.g. plots.yml)")
    parser.add_argument(
        "--histodir",
        help="base path for finding histograms (to be combined with ``'root'`` in configuration and the sample file names; default: the directory that contains yamlFile)",
    )
    parser.add_argument("--eras", type=optStrList, help="Era or (comma-separated) list of eras to consider")
    return parser


def inspectConfig() -> None:
    parser = makeBaseArgsParser(description="Load and interactively inspect a plotIt config")
    parser.add_argument("--verbose", "-v", action="store_true", help="Verbose output")
    args = parser.parse_args()
    import os.path

    if args.histodir:
        histodir = args.histodir
    else:
        histodir = os.path.dirname(args.yamlFile)
    import logging

    logging.basicConfig(level=(logging.DEBUG if args.verbose else logging.INFO))
    config, samples, plotList, systematics, legend = loadFromYAML(args.yamlFile, histodir=histodir, eras=args.eras)

    plots = {p.name: p for p in plotList}  # noqa: F841
    import IPython

    IPython.start_ipython(argv=[], user_ns=vars())


def plotIt_cli() -> None:
    parser = makeBaseArgsParser(description="Python implementation of the plotIt executable (not fully compatible)")
    parser.add_argument("--outdir", "-o", default=".", help="Output directory")
    parser.add_argument("--backend", default="matplotlib", choices=["ROOT", "matplotlib"], help="Backend")
    parser.add_argument("--verbose", "-v", action="store_true", help="Verbose output")
    args = parser.parse_args()
    import os.path

    if args.histodir:
        histodir = args.histodir
    else:
        histodir = os.path.dirname(args.yamlFile)
    import logging

    logging.basicConfig(level=(logging.DEBUG if args.verbose else logging.INFO))
    plotItFromYAML(args.yamlFile, histodir=histodir, outdir=args.outdir, eras=args.eras, backend=args.backend)
